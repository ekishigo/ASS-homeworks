package ass.ekishigo.cvi3.stock.trader.solution;

import ass.ekishigo.cvi3.stock.Stock;
import ass.ekishigo.cvi3.stock.service.IStockService;
import ass.ekishigo.cvi3.stock.service.RandomStockService;
import ass.ekishigo.cvi3.stock.service.SlowStockService;
import ass.ekishigo.cvi3.stock.trader.IStockTrader;
import ass.ekishigo.cvi3.util.ThreadsUtil;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by igorekishev on 18.03.2017.
 * Project cvi3
 */
public class ReactiveTrader implements IStockTrader {
    protected ExecutorService executors = Executors.newFixedThreadPool(10);

    @Override
    public String suggestStockToBuy(IStockService stockService) {
        return Observable.range(0, 10)
                .flatMap(i ->
                        Observable.just(i)
                                .subscribeOn(Schedulers.io())
                                .map((y) -> stockService.next())
                )
                .onErrorReturn(throwable -> {
                    System.out.println("ERROR : " + throwable);
                    return Optional.empty();
                })
                .filter(Optional::isPresent)
                .reduce((stock, stock2) -> stock.get().getValue() <= stock2.get().getValue() ? stock : stock2)
                .blockingGet()
                .orElse(new Stock("N/A", 0f))
                .getCode();
    }
}
