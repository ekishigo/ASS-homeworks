package ass.ekishigo.cvi3.stock.trader.solution;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import ass.ekishigo.cvi3.stock.Stock;
import ass.ekishigo.cvi3.stock.service.IStockService;
import ass.ekishigo.cvi3.stock.service.StockServiceException;
import ass.ekishigo.cvi3.stock.trader.IStockTrader;

import static java.util.stream.IntStream.range;

public class ExecutorTrader implements IStockTrader {

    protected ExecutorService executors = Executors.newFixedThreadPool(10);

    @Override
    public String suggestStockToBuy(IStockService stockService) {
        Callable<Optional<Stock>> nextStock = stockService::next;
        try {
            return executors
                    .invokeAll(range(0, 10)
                            .boxed()
                            .map((Integer i) -> nextStock)
                            .collect(Collectors.toList()))
                    .stream()
                    .map(optionalFuture -> {
                        try {
                            return optionalFuture.get();
                        } catch (InterruptedException | ExecutionException e) {
                            e.printStackTrace();
                            return null;
                        }
                    })
                    .filter(Objects::nonNull)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .min((s1, s2) -> s1.getValue() - s2.getValue() < 0 ? -1 : 1)
                    .orElse(new Stock("N/A", 0f))
                    .getCode();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return "N/A";
        }
    }
}
